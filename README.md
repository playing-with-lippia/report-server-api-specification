# Lippia Report Server API Specification

## Edit Specification File

### Edit from `Swagger Editor` on your local

1. Open you console
2. Run the command
    ```sh
    docker run -d -p 8080:8080 -v $PWD:/tmp -e SWAGGER_FILE=/tmp/swagger.yaml swaggerapi/swagger-editor
    ```
3. Open any web browser
4. Navigate to [Swagger Editor Local](http://127.0.0.1:8080/)
5. Perform your changes
6. Save your changes overriding the existing file

### Edit from `Swagger Editor` cloud

1. Open any web browser
2. Navigate to [Swagger Editor Online](https://editor.swagger.io/)
3. Import file `swagger.yml`
4. Perform your changes
5. Save your changes overriding the existing file


## References
- [Lippia Report Server](https://lippia.io/#report)
- [Swagger](https://swagger.io/)
- [Swagger Editor Online](https://editor.swagger.io/)
